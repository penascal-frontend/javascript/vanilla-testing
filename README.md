# Simple testing approach

## Motivation
This is an example of using an own testing library, with the following objectives:
- Show a little of the magic behind testing libraries
- Show the usefulness of unit tests
- Show how to apply them without having to use any external library or framework.


## How to use it

### The testing library 
It is defined in [`/lib/testing.js`]()
It exports 3 functions:
- `it(string, function)` executes a test defined in `function` and identificable by `string` 
- `assert(condition)` raises error when the `condition` is not trully. This is an auxiliar function that helps to define tests
- `launch(filename)` is a function that simplies the execution of defined specs.

### The environment

The tests are lanched in browser, and are enabled using the namespace added to object `Window`.
This setting can be found in [`/namespaces.js`]() 

### Spec definition
The spec definition must be located in the same directory of the file must be tested.

In example, for testing the file `<file>.js`, the specs must be stored in `<file>.spec.js`.

### Other restrictions
- `namespaces.js` must be imported in main js file. The one defined in HTML `script` tag.
- Each file want to be tested, must have this code in the end of file:
```
if (window.namespaces.testing.enabled) {
  import('/lib/testing.js')
    .then(testing => testing.launch((new Error).fileName))
    .catch(err => console.log(err))
}
```

