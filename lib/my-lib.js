function double(number) {
  return number * 2
}

function upper(text) {
  return text.toLocaleUpperCase()
}

export { double, upper }

if (window.namespaces.testing.enabled) {
  import('/lib/testing.js')
    .then(testing => testing.launch((new Error).fileName))
    .catch(err => console.log(err))
}