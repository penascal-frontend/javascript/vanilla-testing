const styles = {
  error: "color:red;",
  error_bold: "color:red;font-weight:bold;",
  ok: "color:green;font-weight:bold;",
  module: "color:#45A1FF;font-weight:bold;text-transform:uppercase;"
}

function trace(text, style) {
  console.log(`%c${text}`, style)
}


function it(desc, fn) {
  try {
    fn()
    trace(`✔ ${desc}`, styles.ok)
  } catch (error) {
    trace(`✗ ${desc}`, styles.error_bold)
    trace(error, styles.error);
    console.error(error);
  }
}

function assert(condition) {
  if (!condition) {
    throw new Error();
  }
}

function launch(path) {
  const specs = `${path.slice(0,-3)}.spec.js`
  const name = specs.split('/').reverse()[0].split('.').slice(0,-2).join('.')
  console.log(specs)
  import(specs)
    .then(module => {
      trace(`Test set: ${name}`, styles.module)
      module.tests()
    })
    .catch(err => {
      console.log(`Cannot find specs for ${path}`)
      
    })
}

export { it, assert, launch }