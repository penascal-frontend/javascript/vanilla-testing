import { it, assert } from '/lib/testing.js'
import { double, upper } from './my-lib.js'


export function tests() {

  it('should return 2 when 1 is passed as argument', function () {
    assert(double(1) === 2);
  });

  it('should return 4 when 2 is passed as argument', function () {
    assert(double(2) === 4);
  });

  it('should return "ABCD" when "ABCD" is passed as argument', function () {
    assert(upper('abcd') === 'ABCD');
  });

}


