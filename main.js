import './namespace.js'
import { double, upper } from './lib/my-lib.js'

document.getElementById('app').innerText = `
  You can check testing results in console

  ${upper('This is very important')}!!
  
  Double of 5 is ${double(5)}
`
